package startup

type Application struct {
	Name       string   `plist:"-"`
	Identifier string   `plist:"Label"`
	Path       string   `plist:"-"`
	Arguments  []string `plist:"ProgramArguments"`
	RunAtLoad  bool     `plist:"RunAtLoad"`
}
