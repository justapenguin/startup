package startup

import (
	"fmt"
	"os"
	"strings"

	"golang.org/x/sys/windows/registry"
)

const userStartupEntriesKey = `Software\Microsoft\Windows\CurrentVersion\Run`

// SetupLauncher will setup an Application to run on startup if a.RunOnLoad is true
func SetupLauncher(a Application) error {
	key, err := registry.OpenKey(registry.CURRENT_USER, userStartupEntriesKey, registry.WRITE|registry.READ)

	if err != nil {
		return err
	}

	defer key.Close()

	if a.RunAtLoad {
		if a.Path == "" {
			a.Path, err = os.Executable()

			if err != nil {
				return err
			}
		}

		return key.SetStringValue(a.Name, fmt.Sprintf(`"%s" %s`, a.Path, strings.Join(a.Arguments, " ")))
	} else {
		str, _, err := key.GetStringValue(a.Name)

		if err != nil {
			if err == registry.ErrNotExist{
				return nil
			}
			return err
		}
		if str != "" {
			return key.DeleteValue(a.Name)
		} else {
			return nil
		}
	}
}

// WillLaunch determines whether the application is currently set up to launch on startup.
func WillLaunch(a Application) (bool, error) {
	key, err := registry.OpenKey(registry.CURRENT_USER, userStartupEntriesKey, registry.READ)

	if err != nil {
		return false, err
	}

	defer key.Close()

	_, _, err = key.GetStringValue(a.Name)

	if err == registry.ErrNotExist {
		return false, nil
	} else if err != nil {
		return false, err
	}

	return true, nil
}
