package startup

// SetupLauncher will setup an Application to run on startup if a.RunOnLoad is true
func SetupLauncher(a Application) error {
	return nil // @TODO
}

// WillLaunch determines whether the application is currently set up to launch on startup.
func WillLaunch(a Application) (bool, error) {
	return false, nil // @TODO
}
