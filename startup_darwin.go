package startup

import (
	"bytes"
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"
	"strings"

	"howett.net/plist"
)

// SetupLauncher will setup an Application to run on startup if a.RunOnLoad is true
func SetupLauncher(a Application) error {
	var err error
	if a.Path == "" {
		a.Path, err = os.Executable()

		if err != nil {
			return err
		}
	}

	// push the path into the arguments list
	a.Arguments = append([]string{a.Path}, a.Arguments...)

	var buf bytes.Buffer

	enc := plist.NewEncoderForFormat(&buf, plist.XMLFormat)
	enc.Indent("\t")

	if err := enc.Encode(&a); err != nil {
		return err
	}

	str := buf.String()
	str = strings.Replace(str, "<true></true>", "<true/>", -1)
	str = strings.Replace(str, "<false></false>", "<false/>", -1)
	buf.Reset()
	_, err = buf.WriteString(str)

	if err != nil {
		return err
	}

	plistPath, err := getLaunchFile(a)

	if err != nil {
		return err
	}

	return ioutil.WriteFile(plistPath, buf.Bytes(), 0644)
}

func getLaunchFile(a Application) (string, error) {
	u, err := user.Current()

	if err != nil {
		return "", err
	}

	homeDir := u.HomeDir

	return filepath.Join(homeDir, "Library/LaunchAgents", a.Identifier+".plist"), nil
}

// WillLaunch determines whether the application is currently set up to launch on startup.
func WillLaunch(a Application) (bool, error) {
	plistPath, err := getLaunchFile(a)

	if err != nil {
		return false, err
	}

	if _, err := os.Stat(plistPath); os.IsNotExist(err) {
		return false, nil
	} else if err != nil {
		return false, err
	}

	data, err := ioutil.ReadFile(plistPath)

	if err != nil {
		return false, err
	}

	var ap Application

	_, err = plist.Unmarshal(data, &ap)

	if err != nil {
		return false, err
	}

	return ap.RunAtLoad, nil
}
